package com.linkesoft.nextcloudcalendar

import android.app.Application
import android.content.Context

// use class name as TAG in logging
// remove package name (.simpleName does not work for companion objects)
// (won't work with obfuscated code)
val Any.TAG: String get() = (this::class.qualifiedName ?: "").removePrefix(App.appContext.packageName + ".")

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
    }
    companion object {
        lateinit var appContext: Context
    }
}