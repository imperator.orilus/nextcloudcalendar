@file:Suppress("unused")

package com.linkesoft.nextcloudcalendar

import android.app.Activity
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.linkesoft.nextcloudcalendar.data.*
import com.linkesoft.nextcloudcalendar.databinding.ActivityMainBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.LocalDate


@Suppress("unused")
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        // setSupportActionBar(binding.toolbar)

        binding.fab.setOnClickListener {
            if(Prefs.url == null) {
                login()
            } else {
                val nextCloudUrl = Prefs.url + "apps/calendar"
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(nextCloudUrl)
                startActivity(intent)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        Calendar.load()
        refresh()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }


    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        if(menu == null) return false
        if(Prefs.isLoggedIn) {
            // enable menu items
            menu.findItem(R.id.calendars).isVisible = true
            menu.findItem(R.id.refresh).isVisible = true
            menu.findItem(R.id.logout).isVisible = true
            menu.findItem(R.id.login).isVisible = false
            // fill dynamic calendars sub menu
            val calendarsMenu = menu.findItem(R.id.calendars).subMenu!!
            calendarsMenu.clear()
            Calendar.calendars.forEachIndexed { index, calendar ->
                val subMenu = calendarsMenu.add(0, Menu.NONE, Menu.NONE, calendar.displayName)
                subMenu.isCheckable = true
                subMenu.isChecked = calendar.isActive
                subMenu.setOnMenuItemClickListener {
                    Calendar.calendars[index].apply { isActive = !isActive }
                    Calendar.save()
                    refresh()
                    true
                }
            }
        } else {
            // not logged in, show only login button
            menu.findItem(R.id.calendars).isVisible = false
            menu.findItem(R.id.refresh).isVisible = false
            menu.findItem(R.id.login).isVisible = true
            menu.findItem(R.id.logout).isVisible = false
        }
        return super.onPrepareOptionsMenu(menu)
    }

    // simple menu item handlers
    fun refresh(item: MenuItem) {
        refresh()
    }
    fun login(item: MenuItem) {
        login()
    }
    fun logout(item: MenuItem) {
        Prefs.logOut()
        Calendar.calendars = emptyList()
        Calendar.save()
        invalidateOptionsMenu()
        refresh()
    }


    private fun refresh() {
        val today = LocalDate.now()
        binding.todayLabel.text = Calendar.labelFor(getString(R.string.today), today)
        val tomorrow = today.plusDays(1)
        binding.tomorrowLabel.text = Calendar.labelFor(getString(R.string.tomorrow), tomorrow)
        Calendar.load()
        // set old (cached) data
        binding.todayCalendar.text = Calendar.textFor(today)
        binding.tomorrowCalendar.text = Calendar.textFor(tomorrow)

        // get fresh data
        CoroutineScope(Dispatchers.Main).launch {
            if(Prefs.isLoggedIn) {
                Calendar.refresh()
                binding.todayCalendar.text = Calendar.textFor(today)
                binding.tomorrowCalendar.text = Calendar.textFor(tomorrow)
                invalidateOptionsMenu()
            }
            updateWidget()
        }
    }
    private fun updateWidget() {
        val ids = AppWidgetManager.getInstance(this).getAppWidgetIds(ComponentName(this,AppWidget::class.java))
        if(ids.isNotEmpty()) {
            // there are widgets installed on home screen
            val intent = Intent(this, AppWidget::class.java)
            intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
            sendBroadcast(intent)
        }
    }
    private fun login() {
        val editText = EditText(this)
        editText.inputType = InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS
        editText.hint = "https://..."
        editText.setText(Prefs.url ?: "")
        AlertDialog.Builder(this)
            .setTitle("Enter Nextcloud URL")
            .setView(editText)
            .setCancelable(true)
            .setPositiveButton("OK") { _, _ ->
                // TODO valid URL?
                var url = editText.text.toString()
                if(!url.endsWith("/"))
                    url += "/"
                Prefs.url = url
                val intent = Intent(this,LoginActivity::class.java)
                intent.putExtra(Prefs.URL, url)
                handleLogin.launch(intent)
            }
            .show()
        editText.requestFocus()
        (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).showSoftInput(editText, 0)
    }
    private val handleLogin = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if(it.resultCode == Activity.RESULT_OK) {
            val user = it.data!!.getStringExtra(Prefs.USER)!!
            val password = it.data!!.getStringExtra(Prefs.PASSWORD)!!
            // Log.v(TAG, "Got login $user $password")
            Prefs.user = user
            Prefs.password = password
            refresh()
        }
    }
}