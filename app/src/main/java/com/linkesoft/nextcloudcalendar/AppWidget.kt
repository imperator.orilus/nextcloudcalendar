package com.linkesoft.nextcloudcalendar

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.RemoteViews
import com.linkesoft.nextcloudcalendar.data.Calendar
import com.linkesoft.nextcloudcalendar.data.Prefs
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle


/**
 * Implementation of App Widget functionality.
 */
class AppWidget : AppWidgetProvider() {
  override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
    Log.v(TAG, "onUpdate called")
    // to debug the broadcast, remove for release
    // Debug.waitForDebugger()
    // add android:process=":widget" in manifest to debug separately from activities
    val pendingResult = goAsync()
    CoroutineScope(Dispatchers.Main).launch {
      // There may be multiple widgets active, so update all of them
      Calendar.load()
      // first quick update from persisted values
      for (appWidgetId in appWidgetIds) {
        updateAppWidget(context, appWidgetManager, appWidgetId)
      }
      if(Prefs.isLoggedIn) {
        // get fresh data
        Calendar.refresh()
        for (appWidgetId in appWidgetIds) {
          updateAppWidget(context, appWidgetManager, appWidgetId)
        }
      }
      pendingResult?.finish()
    }
  }

  private fun updateAppWidget(context: Context, appWidgetManager: AppWidgetManager, appWidgetId: Int) {
    // Construct the RemoteViews object
    val remoteViews = RemoteViews(context.packageName, R.layout.app_widget)
    val today = LocalDate.now()
    val tomorrow = today.plusDays(1)
    remoteViews.setTextViewText(R.id.todayLabel, Calendar.labelFor(context.getString(R.string.today), today))
    remoteViews.setTextViewText(R.id.tomorrowLabel, Calendar.labelFor(context.getString(R.string.tomorrow), tomorrow))
    remoteViews.setTextViewText(R.id.todayCalendar, Calendar.textFor(today))
    remoteViews.setTextViewText(R.id.tomorrowCalendar, Calendar.textFor(tomorrow))
    remoteViews.setTextViewText(
      R.id.lastUpdate,
      "${context.getString(R.string.lastUpdate)}: ${Calendar.lastUpdate.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT))}"
    )
    // on click, open MainActivity of our app
    val pendingIntent = PendingIntent.getActivity(context, 0, Intent(context, MainActivity::class.java), PendingIntent.FLAG_IMMUTABLE)
    remoteViews.setOnClickPendingIntent(android.R.id.background, pendingIntent)
    // update actual widget in launcher
    appWidgetManager.updateAppWidget(appWidgetId, remoteViews)
  }
}