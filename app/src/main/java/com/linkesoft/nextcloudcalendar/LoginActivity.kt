package com.linkesoft.nextcloudcalendar

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.linkesoft.nextcloudcalendar.data.Prefs
import com.linkesoft.nextcloudcalendar.databinding.ActivityLoginBinding

/**
 * Webview login activity for Nextcloud login
 * @link https://docs.nextcloud.com/server/21/developer_manual/client_apis/LoginFlow/index.html
 */
class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        // required for login form
        binding.webView.settings.javaScriptEnabled = true
        // app name plus device shown to the user on login and stored with the app password on Nextcloud
        binding.webView.settings.userAgentString = "${packageManager.getApplicationLabel(applicationInfo)} ${Build.MANUFACTURER} ${Build.MODEL}"
        // intercept custom url scheme nc: callback
        binding.webView.webViewClient = webViewClient
        // open login URL
        var url = intent.getStringExtra(Prefs.URL) ?: return
        url += "index.php/login/flow"
        Log.v(TAG,"Opening $url")
        // custom header OCS-APIREQUEST required
        binding.webView.loadUrl(url, mapOf("OCS-APIREQUEST" to "true"))
    }

    private val webViewClient = object : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            if(request?.url?.scheme == "nc") {
                // nc://login/server:<server>&user:<loginname>&password:<password>
                // Log.v(TAG,"Callback ${request.url}")
                val regex = """&user:(?<user>\S+)&password:(?<password>\w+)""".toRegex()
                val matchResult = regex.find(request.url.path!!)
                val user = matchResult?.groups?.get("user")?.value
                val password = matchResult?.groups?.get("password")?.value
                if(user != null && password != null) {
                    val result = Intent().apply {
                        putExtra(Prefs.USER,user)
                        putExtra(Prefs.PASSWORD,password)
                    }
                    setResult(Activity.RESULT_OK, result)
                } else {
                    setResult(Activity.RESULT_CANCELED)
                }
                finish()
                return false
            } else {
                // normal redirects
                return super.shouldOverrideUrlLoading(view, request)
            }
        }
    }
}