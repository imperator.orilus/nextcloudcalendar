@file:UseSerializers(LocalDateSerializer::class, LocalDateTimeSerializer::class)

package com.linkesoft.nextcloudcalendar.data

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

@Serializable
class CalendarItem(val startDateTime: LocalDateTime? = null, val endDateTime: LocalDateTime? = null, val startDate: LocalDate? = null, val endDate: LocalDate? = null,
                   val subject: String, val location: String? = null): Comparable<CalendarItem> {

    private val isAllDay get() = startDate != null
    private val isOneDay get() = if(isAllDay) startDate == endDate else startDateTime!!.toLocalDate() == endDateTime!!.toLocalDate()

    fun isOnDate(date: LocalDate): Boolean {
        return if(isAllDay) {
            startDate!! <= date && date <= endDate!!
        } else {
            startDateTime!!.toLocalDate() <= date && date <= endDateTime!!.toLocalDate()
        }
    }

    override fun compareTo(other: CalendarItem): Int = when {
        // all day events first, then ordered by startTime
        this.isAllDay != other.isAllDay -> other.isAllDay compareTo this.isAllDay
        this.isAllDay -> this.startDate!! compareTo other.startDate!!
        else -> this.startDateTime!! compareTo other.startDateTime!!
    }

    override fun toString(): String = when {
        isAllDay && isOneDay -> "${startDate}: $subject"
        isAllDay -> "$startDate - ${endDate}: $subject"
        else -> "$startDateTime - ${endDateTime}: $subject"
    }

    fun formattedString(): String {
        var str = when {
            isAllDay && isOneDay -> ""
            isAllDay -> "${startDate!!.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT))} - ${
                endDate!!.format(
                    DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)
                )
            }: "
            !isOneDay -> "${startDateTime!!.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT))} - ${
                endDateTime!!.format(
                    DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
                )
            }: "
            else -> "${startDateTime!!.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT))} - ${
                endDateTime!!.format(
                    DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)
                )
            }: "
        }
        str += subject
        if(location != null)
            str += " ($location)"
        return str
    }


}

// serialization of LocalDate and LocalDateTime
object LocalDateSerializer : KSerializer<LocalDate> {
    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("LocalDateSerializer", PrimitiveKind.STRING)
    override fun serialize(encoder: Encoder, value: LocalDate) {
        encoder.encodeString(DateTimeFormatter.ISO_DATE.format(value))
    }
    override fun deserialize(decoder: Decoder): LocalDate =
        LocalDate.parse(decoder.decodeString())
}
object LocalDateTimeSerializer : KSerializer<LocalDateTime> {
    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("LocalDateTimeSerializer", PrimitiveKind.STRING)
    override fun serialize(encoder: Encoder, value: LocalDateTime) {
        encoder.encodeString(DateTimeFormatter.ISO_DATE_TIME.format(value))
    }
    override fun deserialize(decoder: Decoder): LocalDateTime =
        LocalDateTime.parse(decoder.decodeString())
}