package com.linkesoft.nextcloudcalendar.data

import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import com.linkesoft.nextcloudcalendar.App

object Prefs {
    const val URL = "url"
    const val USER = "user"
    const val PASSWORD = "password"
    // encrypted preferences, see c't 12/2021 p 122
    // should not be backed up (key is hardware dependent)
    private val sharedPrefs: SharedPreferences = run {
        val masterKey: MasterKey = MasterKey.Builder(App.appContext).setKeyScheme(MasterKey.KeyScheme.AES256_GCM).build()
        EncryptedSharedPreferences.create(App.appContext,"prefs",masterKey,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
    }
    // Nextcloud base URL, ends with /
    var url: String?
        get() {
            return sharedPrefs.getString(URL,null)
        }
        set(value) {
            assert(value == null || value.endsWith("/"))
            sharedPrefs.edit().putString(URL,value).apply()
        }
    var user: String?
        get() {
            return sharedPrefs.getString(USER,null)
        }
        set(value) {
            sharedPrefs.edit().putString(USER,value).apply()
        }
    var password: String?
        get() {
            return sharedPrefs.getString(PASSWORD,null)
        }
        set(value) {
            sharedPrefs.edit().putString(PASSWORD,value).apply()
        }
    val isLoggedIn get() = url != null && user != null && password != null

    fun logOut() {
        url = null
        user = null
        password = null
    }
}