package com.linkesoft.nextcloudcalendar.data

import android.util.Log
import com.linkesoft.nextcloudcalendar.App
import com.linkesoft.nextcloudcalendar.TAG
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import okhttp3.Credentials
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONArray
import org.json.JSONTokener
import org.xml.sax.InputSource
import java.io.File
import java.io.StringReader
import java.time.*
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.time.format.TextStyle
import java.util.*
import javax.xml.parsers.DocumentBuilderFactory

@Serializable
class Calendar(val href: String, val displayName: String) : Comparable<Calendar> {
  var isActive = true
  var calendarItems: List<CalendarItem> = emptyList()

  companion object {
    var calendars: List<Calendar> = emptyList()

    // simple persistence as Json file, should be ok for small number of calendar items
    private val persistenceFile = File(App.appContext.cacheDir, "calendars.json")

    fun save() {
      persistenceFile.parentFile!!.mkdirs()
      Log.v(TAG, "Saving to $persistenceFile")
      persistenceFile.writeText(Json.encodeToString(calendars))
    }

    fun load() {
      Log.v(TAG, "Loading from $persistenceFile")
      calendars =
        if (persistenceFile.exists()) Json.decodeFromString(persistenceFile.readText()) else emptyList()
    }

    val lastUpdate: LocalDateTime
      get() = LocalDateTime.ofInstant(
        Instant.ofEpochMilli(
          persistenceFile.lastModified()
        ), ZoneId.systemDefault()
      )

    fun labelFor(text: String, date: LocalDate): String {
      // Heute: Mo, 28.8.23
      return "$text: ${
            date.dayOfWeek.getDisplayName(
              TextStyle.SHORT,
              Locale.getDefault()
            )
          }, ${date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT))}"
      }

    fun textFor(date: LocalDate): String {
      val text = StringBuilder()
      // collect items from all calendars
      val allItems = ArrayList<CalendarItem>()
      calendars.forEach { calendar ->
        allItems += calendar.calendarItems.filter {it.isOnDate(date)}
      }
      allItems.sorted().forEach { text.appendLine(it.formattedString()) }
      return text.toString()
    }

    // get all calendars from Nextcloud and calendar entries for active calendars
    suspend fun refresh() {
      try {
        calendars =
          withContext(Dispatchers.IO) {
            val calendars = loadCalendars().sorted()
            // keep !isActive flag from existing calendars, new calendars are active by default
            Calendar.calendars.filter { !it.isActive }.forEach { existingCalendar ->
              calendars.first { it.href == existingCalendar.href }.isActive = false
            }
            // get calendar items for active calendars
            calendars.filter { it.isActive }.forEach {
              it.calendarItems = it.loadCalendarEntries()
            }
            return@withContext calendars
          }
        save()
      } catch (exception: Exception) {
        Log.e(TAG, "Could not get calendar data for ${Prefs.url}: ${exception.localizedMessage}", exception)
      }
    }

    // get list of calendar URLs via simple CalDAV call
    private fun loadCalendars(): List<Calendar> {
      val user = Prefs.user ?: return emptyList()
      val password = Prefs.password ?: return emptyList()
      var url = Prefs.url ?: return emptyList()
      url += "remote.php/dav/calendars/" + Prefs.user
      Log.v(TAG, "Calling $url")

      val httpClient = OkHttpClient.Builder().build()
      val body =
        """<d:propfind xmlns:d="DAV:"><d:prop><d:displayname/></d:prop></d:propfind>"""
      val request = Request.Builder().url(url)
        .addHeader("Authorization", Credentials.basic(user, password))
        .method("PROPFIND", body.toRequestBody()).build()
      val response = httpClient.newCall(request).execute()
      val responseBody = response.body?.string() ?: return emptyList()
      response.close()
      Log.v(TAG, "Result $responseBody")
      // parse XML
      /* XML response
  <d:multistatus xmlns:d="DAV:" xmlns:s="http://sabredav.org/ns" xmlns:cal="urn:ietf:params:xml:ns:caldav" xmlns:cs="http://calendarserver.org/ns/" xmlns:oc="http://owncloud.org/ns" xmlns:nc="http://nextcloud.org/ns">
    <d:response>
      <d:href>/remote.php/dav/calendars/<user>/</d:href>
      <d:propstat>
        <d:prop>
          <d:displayname/>
        </d:prop>
        <d:status>HTTP/1.1 404 Not Found</d:status>
      </d:propstat>
    </d:response>
  </d:multistatus>
  */
      val doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(
        InputSource(StringReader(responseBody))
      )
      val hrefs = doc.getElementsByTagName("d:href")
      val displayNames = doc.getElementsByTagName("d:displayname")
      assert(hrefs.length == displayNames.length)
      val result = ArrayList<Calendar>()
      for (i in 0 until hrefs.length) {
        val href = hrefs.item(i).firstChild?.nodeValue
        val displayName = displayNames.item(i).firstChild?.nodeValue
        if (!href.isNullOrEmpty() && !displayName.isNullOrEmpty())
          result.add(Calendar(href, displayName))
      }
      return result
    }

  }

  // get list of timed and untimed calendar entries for a given calendar via Jcal
  private fun loadCalendarEntries(): List<CalendarItem> {
    val user = Prefs.user ?: return emptyList()
    val password = Prefs.password ?: return emptyList()
    var url = Prefs.url ?: return emptyList()
    // get events from yesterday to day after tomorrow to account for timezone differences
    val from = LocalDate.now().minusDays(1).atStartOfDay().toEpochSecond(OffsetDateTime.now().offset)
    val to =
      LocalDate.now().plusDays(2).atStartOfDay().toEpochSecond(OffsetDateTime.now().offset)
    url += href + "?export&expand=1&accept=jcal&start=${from}&end=${to}"
    Log.v(TAG, "Calling $url")
    val httpClient = OkHttpClient.Builder().build()
    val request = Request.Builder().url(url).addHeader("Authorization", Credentials.basic(user, password)).build()
    val response = httpClient.newCall(request).execute()
    val responseBody = response.body?.string() ?: return emptyList()
    response.close()
    Log.v(TAG, "Result $responseBody")
    val jsonArray = JSONTokener(responseBody).nextValue() as? JSONArray ?: return emptyList()
    val vevents = jsonArray[2] as JSONArray
    val result = ArrayList<CalendarItem>()
    for (i in 0 until vevents.length()) {
      val vevent = (vevents[i] as JSONArray)[1] as JSONArray
      val veventMap = map(vevent)
      Log.v(TAG, "Props: $veventMap")
      val dtStart = veventMap["dtstart"] as? String ?: continue
      val dtEnd = veventMap["dtend"] as? String ?: continue
      val subject = veventMap["summary"] as? String ?: continue
      val location = veventMap["location"] as? String
      val calendarItem: CalendarItem
      val allDay = dtStart.length <= 10 // just yyyy-mm-dd
      if (allDay) {
        val startDate = LocalDate.parse(dtStart)
        val endDate = LocalDate.parse(dtEnd).minusDays(1) // end date is always one more as start date
        calendarItem = CalendarItem(startDate = startDate, endDate = endDate, subject = subject, location = location)
      } else {
        // parse date time with time zone, e.g. 2023-08-31T12:30:00Z and store it as a local date time (for current default time zone)
        val startDateTime = ZonedDateTime.parse(dtStart, DateTimeFormatter.ISO_DATE_TIME).withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime()
        val endDateTime = ZonedDateTime.parse(dtEnd, DateTimeFormatter.ISO_DATE_TIME).withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime()
        calendarItem = CalendarItem(startDateTime = startDateTime, endDateTime = endDateTime, subject = subject, location = location
        )
      }
      Log.v(TAG, "CalendarItem: $calendarItem")
      result.add(calendarItem)
    }
    return result
  }

  // convert JSON array of arrays to simple property Map
  // [["dtstart",{},"date","2023-09-01"],["dtend",{},"date","2023-09-02"],["summary",{},"text","Subject"], ... ]
  private fun map(vevent: JSONArray): Map<String, Any> {
    val map = HashMap<String, Any>()
    for (i in 0 until vevent.length()) {
      val prop = vevent[i] as JSONArray
      if (prop.length() > 3) {
        val name = prop[0] as String
        val value = prop[3] as Any
        map[name] = value
      }
    }
    return map
  }
  // simple sort by name
  override fun compareTo(other: Calendar): Int = this.displayName compareTo other.displayName

}